package screenshot;

import com.sun.awt.AWTUtilities;
/*    */ import java.awt.Color;
/*    */ import java.awt.Graphics;
/*    */ import javax.swing.JFrame;

public class Overlay6 extends JFrame
{
    volatile int x1 = -1; volatile int y1 = -1; volatile int x2 = -1; volatile int y2 = -1;

    public Overlay6(ListenThread6 thread) {
        addMouseListener(thread);
        addKeyListener(thread);
        addMouseMotionListener(thread);
        setUndecorated(true);
        AWTUtilities.setWindowOpacity(this, 0.25F);
        setOpacity(0.25F);
    }

    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.black);
        if ((this.x1 > -1) && (this.y1 > -1) && (this.x2 > -1) && (this.y2 > -1))
            g.drawRect(this.x1, this.y1, this.x2 - this.x1, this.y2 - this.y1);
    }

    public void setCoord1(int x, int y)
    {
        this.x1 = x;
        this.y1 = y;
    }

    public void setCoord2(int x, int y) {
        this.x2 = x;
        this.y2 = y;
    }

    public void clearCoords() {
        this.x1 = -1;
        this.y1 = -1;
        this.x2 = -1;
        this.y2 = -1;
    }
}