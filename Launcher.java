package screenshot;

public class Launcher
{
    public static void main(String[] args)
    {
        if (System.getProperty("java.version").substring(0, 3).equals("1.6")) {
            ListenThread6 thread = new ListenThread6();
            thread.start();
        }
        else if (System.getProperty("java.version").substring(0, 3).equals("1.7")) {
            ListenThread7 thread = new ListenThread7();
            thread.start();
        }
    }
}
