package screenshot;

import com.melloware.jintellitype.HotkeyListener;
import com.melloware.jintellitype.JIntellitype;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import javax.imageio.ImageIO;

public class ListenThread6 extends Thread
        implements ActionListener, HotkeyListener, MouseListener, MouseMotionListener, KeyListener
{
    PopupMenu menu = new PopupMenu();
    String tooltip = "Simple Java Screenshotter";
    TrayIcon icon;
    MenuItem options = new MenuItem("Options");
    MenuItem takeShot = new MenuItem("Take Screenshot");
    MenuItem exit = new MenuItem("Exit");
    BufferedImage toolImage;
    Overlay6 frame;
    Point coord1;
    Point coord2;
    Robot robot;
    String appdata = System.getenv("APPDATA");
    File exists = new File(this.appdata, "/Screenshots");
    File x64 = new File("C:/Program Files (x86)");
    volatile boolean mouseDown = false;
    Clipboard clip;

    public ListenThread6()
    {
        if (this.x64.exists())
            JIntellitype.setLibraryLocation("JIntellitype64.dll");
        else
            JIntellitype.setLibraryLocation("JIntellitype.dll");
        if (!this.exists.exists()) {
            this.exists.mkdir();
        }
        JIntellitype.getInstance().registerHotKey(1, 4, 113);
        JIntellitype.getInstance().addHotKeyListener(this);
        this.options.addActionListener(this);
        this.takeShot.addActionListener(this);
        this.exit.addActionListener(this);
        this.menu.add(this.options);
        this.menu.add(this.takeShot);
        this.menu.add(this.exit);
        this.frame = new Overlay6(this);
        this.frame.setBounds(0, 0, Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height);
        this.frame.setBounds(0, 0, 1280, 1024);
        this.frame.setVisible(false);
        this.clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        try {
            this.toolImage = ImageIO.read(new File("camera.png"));
        }
        catch (IOException localIOException) {
        }
        this.icon = new TrayIcon(this.toolImage, this.tooltip, this.menu);
        SystemTray tray = SystemTray.getSystemTray();
        try {
            tray.add(this.icon);
            this.robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() != this.options)
        {
            if (e.getSource() == this.takeShot) {
                this.frame.setVisible(true);
            }
            else if (e.getSource() == this.exit) {
                JIntellitype.getInstance().unregisterHotKey(1);
                JIntellitype.getInstance().removeHotKeyListener(this);
                JIntellitype.getInstance().cleanUp();
                System.exit(0);
            }
        }
    }

    public void onHotKey(int identifier) {
        switch (identifier) {
            case 1:
                this.frame.setVisible(true);
                this.frame.setFocusable(true);
        }
    }

    public void mouseClicked(MouseEvent arg0)
    {
    }

    public void mouseEntered(MouseEvent arg0)
    {
    }

    public void mouseExited(MouseEvent arg0)
    {
    }

    public void mousePressed(MouseEvent arg0) {
        if (arg0.getSource() == this.frame) {
            this.coord1 = new Point(arg0.getXOnScreen(), arg0.getYOnScreen());
            this.mouseDown = true;
            this.frame.setCoord1(arg0.getXOnScreen(), arg0.getYOnScreen());
        }
    }

    public void mouseReleased(MouseEvent arg0) {
        if (arg0.getSource() == this.frame) {
            this.coord2 = new Point(arg0.getXOnScreen(), arg0.getYOnScreen());
            this.frame.setVisible(false);
            this.mouseDown = false;
            try {
                BufferedImage shot = takeShot();
                ImageIO.write(shot, "png", new File(this.exists, System.currentTimeMillis() + ".png"));
                String reading = uploadShot(shot);
                int in = reading.indexOf("http://i.imgur.com/");
                String imageURL = reading.substring(in, in + 28);
                StringSelection clipBoardURL = new StringSelection(imageURL);
                this.clip.setContents(clipBoardURL, clipBoardURL);
                this.icon.displayMessage("Image uploaded!", imageURL.toString(), TrayIcon.MessageType.NONE);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.coord1 = null;
            this.coord2 = null;
            this.frame.clearCoords();
            this.frame.repaint();
        }
    }

    public void mouseDragged(MouseEvent arg0) {
        if ((arg0.getSource() == this.frame) &&
                (this.coord1 != null) && (this.mouseDown)) {
            this.frame.setCoord2(arg0.getXOnScreen(), arg0.getYOnScreen());
            this.frame.repaint();
        }
    }

    public void mouseMoved(MouseEvent arg0)
    {
    }

    public void keyPressed(KeyEvent arg0)
    {
    }

    public void keyReleased(KeyEvent arg0)
    {
        if ((arg0.getSource() == this.frame) &&
                (arg0.getKeyCode() == 27)) {
            this.frame.setVisible(false);
            this.coord1 = null;
            this.coord2 = null;
            this.frame.clearCoords();
        }
    }

    public void keyTyped(KeyEvent arg0)
    {
    }

    public BufferedImage takeShot()
    {
        return this.robot.createScreenCapture(new Rectangle(this.coord1.x, this.coord1.y, this.coord2.x - this.coord1.x, this.coord2.y - this.coord1.y));
    }

    public String uploadShot(Image shot) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String reading = "error";
        try
        {
            ImageIO.write((BufferedImage)shot, "png", baos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String data = URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(Base64.encode(baos.toByteArray()), "UTF-8");
            data = data + "&" + URLEncoder.encode("key", "UTF-8") + "=" + URLEncoder.encode("ac46d471f521a8805ff4e7051635cd89", "UTF-8");
            URL url = new URL("http://api.imgur.com/2/upload");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.connect();
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            BufferedReader read = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            reading = read.readLine();
            reading = read.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reading;
    }
}